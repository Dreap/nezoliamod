
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.Item;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Set;
import java.util.HashMap;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemAmethysteHoe extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:amethystehoe")
	public static final Item block = null;
	public ItemAmethysteHoe(ElementsNezoliamodMod instance) {
		super(instance, 32);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemHoe(EnumHelper.addToolMaterial("AMETHYSTEHOE", 2, 450, 5.5f, 0f, 14)) {
			public Set<String> getToolClasses(ItemStack stack) {
				HashMap<String, Integer> ret = new HashMap<String, Integer>();
				ret.put("hoe", 2);
				return ret.keySet();
			}
		}.setUnlocalizedName("amethystehoe").setRegistryName("amethystehoe").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("nezoliamod:amethystehoe", "inventory"));
	}
}
