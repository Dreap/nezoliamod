
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemNezoliteArmor extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:nezolitearmorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitearmorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitearmorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitearmorboots")
	public static final Item boots = null;
	public ItemNezoliteArmor(ElementsNezoliamodMod instance) {
		super(instance, 19);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("NEZOLITEARMOR", "nezoliamod:nezolite_", 7, new int[]{4, 5, 7, 6}, 30,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("nezolitearmorhelmet")
				.setRegistryName("nezolitearmorhelmet").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("nezolitearmorbody")
				.setRegistryName("nezolitearmorbody").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("nezolitearmorlegs")
				.setRegistryName("nezolitearmorlegs").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("nezolitearmorboots")
				.setRegistryName("nezolitearmorboots").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("nezoliamod:nezolitearmorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("nezoliamod:nezolitearmorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("nezoliamod:nezolitearmorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("nezoliamod:nezolitearmorboots", "inventory"));
	}
}
