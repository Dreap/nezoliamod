
package net.mcreator.nezoliamod.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.nezoliamod.item.ItemNezoliteRenforceGem;
import net.mcreator.nezoliamod.block.BlockNezoliteRenforceOre;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class RecipeNezoliteRenforceOreSmelting extends ElementsNezoliamodMod.ModElement {
	public RecipeNezoliteRenforceOreSmelting(ElementsNezoliamodMod instance) {
		super(instance, 84);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockNezoliteRenforceOre.block, (int) (1)), new ItemStack(ItemNezoliteRenforceGem.block, (int) (1)),
				0.7F);
	}
}
