
package net.mcreator.nezoliamod.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.nezoliamod.item.ItemNezoliteJauneGem;
import net.mcreator.nezoliamod.block.BlockNezoliteJauneOre;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class RecipeNezoliteJauneOreSmelting extends ElementsNezoliamodMod.ModElement {
	public RecipeNezoliteJauneOreSmelting(ElementsNezoliamodMod instance) {
		super(instance, 72);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockNezoliteJauneOre.block, (int) (1)), new ItemStack(ItemNezoliteJauneGem.block, (int) (1)), 0.7F);
	}
}
