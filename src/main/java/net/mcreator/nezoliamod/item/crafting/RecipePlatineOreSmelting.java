
package net.mcreator.nezoliamod.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.nezoliamod.item.ItemPlatineGem;
import net.mcreator.nezoliamod.block.BlockPlatineOre;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class RecipePlatineOreSmelting extends ElementsNezoliamodMod.ModElement {
	public RecipePlatineOreSmelting(ElementsNezoliamodMod instance) {
		super(instance, 108);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockPlatineOre.block, (int) (1)), new ItemStack(ItemPlatineGem.block, (int) (1)), 0.7F);
	}
}
