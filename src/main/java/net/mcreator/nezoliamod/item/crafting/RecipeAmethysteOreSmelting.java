
package net.mcreator.nezoliamod.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.nezoliamod.item.ItemAmethysteGem;
import net.mcreator.nezoliamod.block.BlockAmethysteOre;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class RecipeAmethysteOreSmelting extends ElementsNezoliamodMod.ModElement {
	public RecipeAmethysteOreSmelting(ElementsNezoliamodMod instance) {
		super(instance, 96);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockAmethysteOre.block, (int) (1)), new ItemStack(ItemAmethysteGem.block, (int) (1)), 0.7F);
	}
}
