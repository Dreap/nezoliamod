
package net.mcreator.nezoliamod.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.nezoliamod.item.ItemNezoliteGem;
import net.mcreator.nezoliamod.block.BlockNezoliteOre;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class RecipeNezoliteOreSmelting extends ElementsNezoliamodMod.ModElement {
	public RecipeNezoliteOreSmelting(ElementsNezoliamodMod instance) {
		super(instance, 59);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockNezoliteOre.block, (int) (1)), new ItemStack(ItemNezoliteGem.block, (int) (1)), 0.7F);
	}
}
