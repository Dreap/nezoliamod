
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemNezoliteJauneArmor extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:nezolitejaunearmorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitejaunearmorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitejaunearmorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezolitejaunearmorboots")
	public static final Item boots = null;
	public ItemNezoliteJauneArmor(ElementsNezoliamodMod instance) {
		super(instance, 1);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("NEZOLITEJAUNEARMOR", "nezoliamod:nezolite_jaune_", 6, new int[]{4, 5, 5, 4}, 30,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 3f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("nezolitejaunearmorhelmet")
				.setRegistryName("nezolitejaunearmorhelmet").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("nezolitejaunearmorbody")
				.setRegistryName("nezolitejaunearmorbody").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("nezolitejaunearmorlegs")
				.setRegistryName("nezolitejaunearmorlegs").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("nezolitejaunearmorboots")
				.setRegistryName("nezolitejaunearmorboots").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("nezoliamod:nezolitejaunearmorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("nezoliamod:nezolitejaunearmorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("nezoliamod:nezolitejaunearmorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("nezoliamod:nezolitejaunearmorboots", "inventory"));
	}
}
