
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.procedure.ProcedureArmorFarmHelmetTickEvent;
import net.mcreator.nezoliamod.procedure.ProcedureArmorFarmBodyTickEventLeggingsTickEvent;
import net.mcreator.nezoliamod.procedure.ProcedureArmorFarmBodyTickEventBootsTickEvent;
import net.mcreator.nezoliamod.procedure.ProcedureArmorFarmBodyTickEvent;
import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;
import java.util.HashMap;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemArmorFarm extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:armorfarmhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("nezoliamod:armorfarmbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("nezoliamod:armorfarmlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("nezoliamod:armorfarmboots")
	public static final Item boots = null;
	public ItemArmorFarm(ElementsNezoliamodMod instance) {
		super(instance, 141);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("ARMORFARM", "nezoliamod:farm_", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD) {
			@Override
			public void onArmorTick(World world, EntityPlayer entity, ItemStack itemstack) {
				super.onArmorTick(world, entity, itemstack);
				int x = (int) entity.posX;
				int y = (int) entity.posY;
				int z = (int) entity.posZ;
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					ProcedureArmorFarmHelmetTickEvent.executeProcedure($_dependencies);
				}
			}
		}.setUnlocalizedName("armorfarmhelmet").setRegistryName("armorfarmhelmet").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST) {
			@Override
			public void onArmorTick(World world, EntityPlayer entity, ItemStack itemstack) {
				int x = (int) entity.posX;
				int y = (int) entity.posY;
				int z = (int) entity.posZ;
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					ProcedureArmorFarmBodyTickEvent.executeProcedure($_dependencies);
				}
			}
		}.setUnlocalizedName("armorfarmbody").setRegistryName("armorfarmbody").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS) {
			@Override
			public void onArmorTick(World world, EntityPlayer entity, ItemStack itemstack) {
				int x = (int) entity.posX;
				int y = (int) entity.posY;
				int z = (int) entity.posZ;
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					ProcedureArmorFarmBodyTickEventLeggingsTickEvent.executeProcedure($_dependencies);
				}
			}
		}.setUnlocalizedName("armorfarmlegs").setRegistryName("armorfarmlegs").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET) {
			@Override
			public void onArmorTick(World world, EntityPlayer entity, ItemStack itemstack) {
				int x = (int) entity.posX;
				int y = (int) entity.posY;
				int z = (int) entity.posZ;
				{
					Map<String, Object> $_dependencies = new HashMap<>();
					$_dependencies.put("entity", entity);
					ProcedureArmorFarmBodyTickEventBootsTickEvent.executeProcedure($_dependencies);
				}
			}
		}.setUnlocalizedName("armorfarmboots").setRegistryName("armorfarmboots").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("nezoliamod:armorfarmhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("nezoliamod:armorfarmbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("nezoliamod:armorfarmlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("nezoliamod:armorfarmboots", "inventory"));
	}
}
