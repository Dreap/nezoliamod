
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemAmethysteArmor extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:amethystearmorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("nezoliamod:amethystearmorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("nezoliamod:amethystearmorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("nezoliamod:amethystearmorboots")
	public static final Item boots = null;
	public ItemAmethysteArmor(ElementsNezoliamodMod instance) {
		super(instance, 28);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("AMETHYSTEARMOR", "nezoliamod:amethyste_", 7, new int[]{4, 4, 6, 3}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("amethystearmorhelmet")
				.setRegistryName("amethystearmorhelmet").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("amethystearmorbody")
				.setRegistryName("amethystearmorbody").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("amethystearmorlegs")
				.setRegistryName("amethystearmorlegs").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("amethystearmorboots")
				.setRegistryName("amethystearmorboots").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("nezoliamod:amethystearmorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("nezoliamod:amethystearmorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("nezoliamod:amethystearmorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("nezoliamod:amethystearmorboots", "inventory"));
	}
}
