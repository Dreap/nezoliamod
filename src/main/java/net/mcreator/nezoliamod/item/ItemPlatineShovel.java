
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.Item;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Set;
import java.util.HashMap;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemPlatineShovel extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:platineshovel")
	public static final Item block = null;
	public ItemPlatineShovel(ElementsNezoliamodMod instance) {
		super(instance, 42);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemSpade(EnumHelper.addToolMaterial("PLATINESHOVEL", 2, 750, 7f, -2f, 30)) {
			{
				this.attackSpeed = -3f;
			}
			public Set<String> getToolClasses(ItemStack stack) {
				HashMap<String, Integer> ret = new HashMap<String, Integer>();
				ret.put("spade", 2);
				return ret.keySet();
			}
		}.setUnlocalizedName("platineshovel").setRegistryName("platineshovel").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("nezoliamod:platineshovel", "inventory"));
	}
}
