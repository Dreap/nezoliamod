
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemNezoliteRenforceArmor extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:nezoliterenforcearmorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezoliterenforcearmorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezoliterenforcearmorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("nezoliamod:nezoliterenforcearmorboots")
	public static final Item boots = null;
	public ItemNezoliteRenforceArmor(ElementsNezoliamodMod instance) {
		super(instance, 10);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("NEZOLITERENFORCEARMOR", "nezoliamod:nezolite_renforcee_", 7,
				new int[]{4, 5, 7, 5}, 30, (net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")),
				0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("nezoliterenforcearmorhelmet")
				.setRegistryName("nezoliterenforcearmorhelmet").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("nezoliterenforcearmorbody")
				.setRegistryName("nezoliterenforcearmorbody").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("nezoliterenforcearmorlegs")
				.setRegistryName("nezoliterenforcearmorlegs").setCreativeTab(TabNezolia.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("nezoliterenforcearmorboots")
				.setRegistryName("nezoliterenforcearmorboots").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("nezoliamod:nezoliterenforcearmorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("nezoliamod:nezoliterenforcearmorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("nezoliamod:nezoliterenforcearmorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("nezoliamod:nezoliterenforcearmorboots", "inventory"));
	}
}
