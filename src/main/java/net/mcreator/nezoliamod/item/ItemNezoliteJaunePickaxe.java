
package net.mcreator.nezoliamod.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.Item;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.nezoliamod.creativetab.TabNezolia;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Set;
import java.util.HashMap;

@ElementsNezoliamodMod.ModElement.Tag
public class ItemNezoliteJaunePickaxe extends ElementsNezoliamodMod.ModElement {
	@GameRegistry.ObjectHolder("nezoliamod:nezolitejaunepickaxe")
	public static final Item block = null;
	public ItemNezoliteJaunePickaxe(ElementsNezoliamodMod instance) {
		super(instance, 3);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemPickaxe(EnumHelper.addToolMaterial("NEZOLITEJAUNEPICKAXE", 2, 1200, 7f, -2f, 30)) {
			{
				this.attackSpeed = -3f;
			}
			public Set<String> getToolClasses(ItemStack stack) {
				HashMap<String, Integer> ret = new HashMap<String, Integer>();
				ret.put("pickaxe", 2);
				return ret.keySet();
			}
		}.setUnlocalizedName("nezolitejaunepickaxe").setRegistryName("nezolitejaunepickaxe").setCreativeTab(TabNezolia.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("nezoliamod:nezolitejaunepickaxe", "inventory"));
	}
}
