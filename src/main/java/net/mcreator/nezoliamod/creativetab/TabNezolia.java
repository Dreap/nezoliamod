
package net.mcreator.nezoliamod.creativetab;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import net.mcreator.nezoliamod.item.ItemLegendaryFireSworld;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

@ElementsNezoliamodMod.ModElement.Tag
public class TabNezolia extends ElementsNezoliamodMod.ModElement {
	public TabNezolia(ElementsNezoliamodMod instance) {
		super(instance, 69);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabnezolia") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemLegendaryFireSworld.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return true;
			}
		}.setBackgroundImageName("item_search.png");
	}
	public static CreativeTabs tab;
}
