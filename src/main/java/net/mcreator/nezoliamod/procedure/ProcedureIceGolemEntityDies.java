package net.mcreator.nezoliamod.procedure;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import net.mcreator.nezoliamod.item.ItemIceFragment;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;

@ElementsNezoliamodMod.ModElement.Tag
public class ProcedureIceGolemEntityDies extends ElementsNezoliamodMod.ModElement {
	public ProcedureIceGolemEntityDies(ElementsNezoliamodMod instance) {
		super(instance, 118);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure IceGolemEntityDies!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((5 > Math.random())) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(ItemIceFragment.block, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
	}
}
