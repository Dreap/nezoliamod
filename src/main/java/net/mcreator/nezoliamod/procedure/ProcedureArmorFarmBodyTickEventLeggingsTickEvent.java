package net.mcreator.nezoliamod.procedure;

import net.minecraft.potion.PotionEffect;
import net.minecraft.init.MobEffects;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;

@ElementsNezoliamodMod.ModElement.Tag
public class ProcedureArmorFarmBodyTickEventLeggingsTickEvent extends ElementsNezoliamodMod.ModElement {
	public ProcedureArmorFarmBodyTickEventLeggingsTickEvent(ElementsNezoliamodMod instance) {
		super(instance, 143);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure ArmorFarmBodyTickEventLeggingsTickEvent!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.SPEED, (int) 60, (int) 1));
	}
}
