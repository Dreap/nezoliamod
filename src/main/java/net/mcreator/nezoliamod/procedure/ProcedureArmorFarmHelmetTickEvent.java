package net.mcreator.nezoliamod.procedure;

import net.minecraft.potion.PotionEffect;
import net.minecraft.init.MobEffects;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;

@ElementsNezoliamodMod.ModElement.Tag
public class ProcedureArmorFarmHelmetTickEvent extends ElementsNezoliamodMod.ModElement {
	public ProcedureArmorFarmHelmetTickEvent(ElementsNezoliamodMod instance) {
		super(instance, 141);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure ArmorFarmHelmetTickEvent!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.NIGHT_VISION, (int) 500, (int) 2));
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.HASTE, (int) 500, (int) 2));
	}
}
