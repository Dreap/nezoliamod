package net.mcreator.nezoliamod.procedure;

import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.world.World;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.math.BlockPos;
import net.minecraft.item.ItemStack;
import net.minecraft.init.Blocks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import net.mcreator.nezoliamod.item.ItemAmethystePickaxe;
import net.mcreator.nezoliamod.gui.GuiMineurNIV1;
import net.mcreator.nezoliamod.block.BlockNezoliteRenforceOre;
import net.mcreator.nezoliamod.block.BlockNezoliteOre;
import net.mcreator.nezoliamod.block.BlockAmethysteOre;
import net.mcreator.nezoliamod.NezoliamodMod;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;

@ElementsNezoliamodMod.ModElement.Tag
public class ProcedureMineurJobs extends ElementsNezoliamodMod.ModElement {
	public ProcedureMineurJobs(ElementsNezoliamodMod instance) {
		super(instance, 128);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure MineurJobs!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure MineurJobs!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure MineurJobs!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure MineurJobs!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure MineurJobs!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if (((world.getBlockState(new BlockPos((int) x, (int) y, (int) z))).getBlock() == Blocks.STONE.getStateFromMeta(0).getBlock())) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("+1  xp pour le metier de mineur"), (true));
			}
			entity.getEntityData().setDouble("mineur", ((entity.getEntityData().getDouble("mineur")) + 1));
		}
		if (((world.getBlockState(new BlockPos((int) x, (int) y, (int) z))).getBlock() == BlockAmethysteOre.block.getDefaultState().getBlock())) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("+3  xp pour le metier de mineur"), (true));
			}
			entity.getEntityData().setDouble("mineur", ((entity.getEntityData().getDouble("mineur")) + 3));
		}
		if (((world.getBlockState(new BlockPos((int) x, (int) y, (int) z))).getBlock() == BlockNezoliteOre.block.getDefaultState().getBlock())) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("+3  xp pour le metier de mineur"), (true));
			}
			entity.getEntityData().setDouble("mineur", ((entity.getEntityData().getDouble("mineur")) + 5));
		}
		if (((world.getBlockState(new BlockPos((int) x, (int) y, (int) z))).getBlock() == BlockNezoliteRenforceOre.block.getDefaultState()
				.getBlock())) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("+15  xp pour le metier de mineur"), (true));
			}
			entity.getEntityData().setDouble("mineur", ((entity.getEntityData().getDouble("mineur")) + 15));
		}
		if (((entity.getEntityData().getDouble("mineur")) == 100)) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("\u00A76Nezolia : \u00A73Tu es mineur de Lvl 1"), (false));
			}
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(ItemAmethystePickaxe.block, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
			if (entity instanceof EntityPlayer)
				((EntityPlayer) entity).openGui(NezoliamodMod.instance, GuiMineurNIV1.GUIID, world, x, y, z);
		}
		if (((entity.getEntityData().getDouble("mineur")) == 400)) {
			if (entity instanceof EntityPlayer && !entity.world.isRemote) {
				((EntityPlayer) entity).sendStatusMessage(new TextComponentString("\u00A76Nezolia : \u00A73Tu es mineur de Lvl 2"), (true));
			}
			if (entity instanceof EntityPlayer)
				((EntityPlayer) entity).addExperienceLevel((int) 25);
		}
	}

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event) {
		Entity entity = event.getPlayer();
		java.util.HashMap<String, Object> dependencies = new java.util.HashMap<>();
		dependencies.put("xpAmount", event.getExpToDrop());
		dependencies.put("x", (int) event.getPos().getX());
		dependencies.put("y", (int) event.getPos().getY());
		dependencies.put("z", (int) event.getPos().getZ());
		dependencies.put("px", (int) entity.posX);
		dependencies.put("py", (int) entity.posY);
		dependencies.put("pz", (int) entity.posZ);
		dependencies.put("world", event.getWorld());
		dependencies.put("entity", entity);
		dependencies.put("event", event);
		this.executeProcedure(dependencies);
	}

	@Override
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(this);
	}
}
