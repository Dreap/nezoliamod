package net.mcreator.nezoliamod.procedure;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Items;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import net.mcreator.nezoliamod.item.ItemNezoliteRenforceGem;
import net.mcreator.nezoliamod.item.ItemNezoliteJauneGem;
import net.mcreator.nezoliamod.item.ItemNezoliteGem;
import net.mcreator.nezoliamod.ElementsNezoliamodMod;

import java.util.Map;

@ElementsNezoliamodMod.ModElement.Tag
public class ProcedureRamdomOreBlockDestroyedByPlayer extends ElementsNezoliamodMod.ModElement {
	public ProcedureRamdomOreBlockDestroyedByPlayer(ElementsNezoliamodMod instance) {
		super(instance, 151);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure RamdomOreBlockDestroyedByPlayer!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((Math.random() < 0.7)) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(ItemNezoliteRenforceGem.block, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
		if ((Math.random() < 0.5)) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(ItemNezoliteGem.block, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
		if ((Math.random() < 0.1)) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(ItemNezoliteJauneGem.block, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
		if ((Math.random() < 0.7)) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(Items.DIAMOND, (int) (1));
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
		if ((Math.random() < 0.7)) {
			if (entity instanceof EntityPlayer) {
				ItemStack _setstack = new ItemStack(Items.COAL, (int) (1), 0);
				_setstack.setCount(1);
				ItemHandlerHelper.giveItemToPlayer(((EntityPlayer) entity), _setstack);
			}
		}
	}
}
